import React from 'react';
import styled from 'styled-components';

const StyledDiv = styled.div`
  background-color: white;
  border-radius: 10px;
  color: black;
  font: inherit;
  margin-top:8px;
  margin-bottom: 8px;
  border: 1px solid black;
  padding: 8px;
  cursor: pointer;
`;

const StyledT = styled.div`
  border-radius: 10px;
  color: black;
  font: inherit;
  text-align: left;
  font-size: 20px;
  padding: 8px;
  margin: 1px;
  cursor: pointer;
`;

const StyledD = styled.div`
  background-color: salmon;
  border-radius: 10px;
  color: black;
  font: inherit;
  text-align: left;
  font-size: 20px;
  padding: 8px;
  border: 1px solid black;
  margin: 1px;
  cursor: pointer;
`;

const StyledA = styled.div`
  background-color: #3E7FD4;
  display: inline-block;
  margin: 1px;
  text-align: center;
  border-radius: 10px;
  color: black;
  font: inherit;
  width: 180px;
  font-size: 12px;
  border: 1px solid black;
  padding: 3px;
  cursor: pointer;
`;

const StyledR = styled.div`
  background-color: #F2A100;
  margin: 1px;
  text-align: center;
  border-radius: 10px;
  vertical-align:top;
  display: inline-block;
  color: black;
  font: inherit;
  width: 180px;
  font-size: 12px;
  border: 1px solid black;
  padding: 3px;
  cursor: pointer;
`;

const StyledDa= styled.div`
  background-color: #76F140;
  margin: 1px;
  text-align: center;
  border-radius: 10px;
  vertical-align:top;
  display: inline-block;
  color: black;
  font: inherit;
  width: 180px;
  font-size: 12px;
  border: 1px solid black;
  padding: 3px;
  cursor: pointer;
`;

const ticketDetails = () =>{
    return (
        <div>
            <StyledDiv>
                <StyledT>
                    <p>Task name: This is a task example</p>
                    <StyledA>
                        <p>Marco</p>
                    </StyledA>
                    <StyledR>
                        <p>Carlos</p>
                    </StyledR>
                    <StyledDa>
                        <p>10/25/2020</p>
                    </StyledDa>
                    <p> Description: </p>
                    <StyledD> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</StyledD>
                </StyledT>
            </StyledDiv>
        </div>
    )
    };
    
export default ticketDetails;