import React from 'react';
import styled from 'styled-components';

const StyledDiv = styled.div`
  background-color: white;
  border-radius: 10px;
  color: black;
  font: inherit;
  margin-top:8px;
  margin-bottom: 8px;
  border: 1px solid black;
  padding: 8px;
  cursor: pointer;
  &:hover {
    background-color: purple;
    color: black;
  }
`;

const StyledT = styled.div`
  background-color: #F2A100;
  border-radius: 10px;
  display: inline-block;
  width:120px;
  color: black;
  font: inherit;
  font-size: 12px;
  border: 1px solid black;
  padding: 2px;
  margin: 1px;
  cursor: pointer;
`;

const StyledA = styled.div`
  background-color: #3E7FD4;
  margin: 1px;
  border-radius: 10px;
  display: inline-block;
  width:120px;
  vertical-align:top;
  color: black;
  font: inherit;
  font-size: 12px;
  border: 1px solid black;
  padding: 2px;
  cursor: pointer;
`;

const ticket = (props) =>{
return (
    <div>
        <StyledDiv onClick = {props.click}>
            <StyledT>
                <p>{props.title}</p>
            </StyledT>
            <StyledA>
                <p>{props.assignee}</p>
            </StyledA>
        </StyledDiv>
    </div>
)
};

export default ticket;