import React, {Component} from 'react';
import { Redirect } from "react-router-dom";
import Ticket from '../Ticket/Ticket'
import '../App.css';
import styled from 'styled-components';

class Home extends Component {

  state = {
    tickets: [
      {id: 'hsdfkhfs', title: 'Add styles to all components', requester: 'Carlos', assignee: 'Marco', date: '10/25/2020'},
      {id: 'sfdvtr', title: 'Create new repo', requester: 'Jonathan', assignee: 'Carlos', date: ''},
      {id: 'kfgrild', title: 'Approve Pull Requests', requester: 'Marco', assignee: 'Leticia', date: ''},
      {id: 'ksdbgekf', title: 'Create firestore db', requester: 'Leticia', assignee: 'Jonathan', date: ''}
    ],
    showTickets : true,
    changePage: false
  }

  handleRedirect = () =>{
    this.setState({changePage: true})
    console.log('this is working')
  }

  render(){

    let redirectHelper = null;

    let tickets = null;

    if(this.state.showTickets){
      tickets = (
        <div>
          {this.state.tickets.map((ticket) => {
            return <Ticket
              click = {this.handleRedirect}
              title = {ticket.title}
              key = {ticket.id}
              assignee = {ticket.assignee}
              //changed = {(event)=> this.nameChangedHandler(event, person.id)}
              date = {ticket.date}/>
          })}
      </div>
      );
    }

    const StyledDiv = styled.div`
      background-color: #43484d ;
      border-radius: 20px;
      color: white;
      font: inherit;
      border: 1px solid black;
      padding: 8px;
      cursor: pointer;
    `;

    if(this.state.changePage){
      redirectHelper = (
        <Redirect to= '/details' />
      );
    }
    
    return (
        <div className="App">
            <meta charSet="utf-8"/>
            <script defer src="https://use.fontawesome.com/releases/v5.14.0/js/all.js"></script>
            <meta name="viewport" content="width=device-width, initial-scale=1"/>
            <title>Hello Bulma!</title>
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css"/>
            <section className="section">
              <div className="container">
                <h1 className="title">
                  Trello Clone
                </h1>
                <div className="columns">
                  <div className="column">TODO
                    <StyledDiv>
                      {tickets}
                    </StyledDiv>
                  </div>
                  <div className="column">In Progress
                    <StyledDiv>
                      <Ticket title = {'task in progress'} assignee = {'Don Ramón'} />
                    </StyledDiv>    
                  </div>
                  <div className="column">DONE
                    <StyledDiv>
                      <Ticket title = {'finished task'} assignee = {'Hackerman'} />
                      <Ticket title = {'finished task'} assignee = {'Someone'} />
                    </StyledDiv>
                  </div>
                </div>
              </div>
            </section>
            {redirectHelper}
        </div>
    );
  }
  
}

export default Home;
