import React from 'react';
import Home from './Home/Home'
import Details from './Details/Details'
import { BrowserRouter as Router, Route } from 'react-router-dom';

function App() {
  return (
      <Router>
        <div>
          <Route exact path ="/" component = {Home} />
          <Route exact path ="/details" component = {Details} />
        </div>
      </Router>
  );
};

export default App;