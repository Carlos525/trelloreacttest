import React, {Component} from 'react';
import '../App.css';
import TicketDetails from '../TicketDetails/TicketDetails'
import styled from 'styled-components';

class Details extends Component {

  render(){

    const StyledDiv = styled.div`
      background-color: #43484d ;
      border-radius: 20px;
      margin-left: 250px;
      margin-right: 250px;
      margin-top: 20px;
      color: white;
      font: inherit;
      border: 1px solid black;
      padding: 8px;
      cursor: pointer;
    `;
    
    return (
        <div className="App">
            <section className="section">
              <div className="container">
                <h1 className="title">
                  Trello Clone
                </h1>
                <div className="columns">
                  <div className="column">Task
                    <StyledDiv>
                        <TicketDetails/>
                    </StyledDiv>
                  </div>
                </div>
              </div>
            </section>
        </div>
    );
  }
  
}

export default Details;